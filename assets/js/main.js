$(document).ready(function () {

    $(".maplocation_place h5").click(function () {
        $(".award-map").addClass("shadeover");
    }), $(".awardspopup_close").click(function () {
        $(".award-map").removeClass("shadeover");
    }), $(".scotland_map h5").click(function () {
        $(".f1_list").show(), $(".f2_list").hide(), $(".f4_list").hide(), $(".f3_list").hide(), $(".f5_list").hide(), $(".f6_list").hide(), $(".f7_list").hide()
    }), $(".uk_map h5").click(function () {
        $(".f6_list").show(), $(".f1_list").hide(), $(".f2_list").hide(), $(".f3_list").hide(), $(".f4_list").hide(), $(".f5_list").hide(), $(".f7_list").hide()
    }), $(".vietnam_map h5").click(function () {
        $(".f7_list").show(), $(".f1_list").hide(), $(".f2_list").hide(), $(".f3_list").hide(), $(".f4_list").hide(), $(".f5_list").hide(), $(".f6_list").hide()
    }), $(".bangladesh_map h5").click(function () {
        $(".f4_list").show(), $(".f1_list").hide(), $(".f2_list").hide(), $(".f3_list").hide(), $(".f5_list").hide(), $(".f6_list").hide(), $(".f7_list").hide()
    }), $(".pakistan_map h5").click(function () {
        $(".f5_list").show(), $(".f1_list").hide(), $(".f2_list").hide(), $(".f3_list").hide(), $(".f4_list").hide(), $(".f6_list").hide(), $(".f7_list").hide()
    }), $(".indonesia_map h5").click(function () {
        $(".f1_list").hide(), $(".f2_list").show(), $(".f4_list").hide(), $(".f3_list").hide(), $(".f5_list").hide(), $(".f6_list").hide(), $(".f7_list").hide()
    }), $(".india_map h5").click(function () {
        $(".f1_list").hide(), $(".f2_list").hide(), $(".f4_list").hide(), $(".f3_list").show(), $(".f5_list").hide(), $(".f6_list").hide(), $(".f7_list").hide()
    }), $(".freedemo_pop").click(function () {
        $(".cont_book_chat_listinner.openclick").trigger("click")
    }), $(".newspop").on("click", function () {
        var e = $(this).find("img").attr("src");
        e = e.replace("news_print/", "news_print/large/"), $(".imagepreview").attr("src", e), $("#imagemodal").modal("show")
    }), $(document).on("click", ".maplocation_place h5", function () {
        var e = $(this).attr("data-name");
        $(this).parents(".maplocation_place").attr("data-list") - 1;
        $(".awardspopup_info").fadeIn(), $(".awardspopup_info .awards_popcnt." + e).fadeIn()
    }), $(document).on("click", ".awardspopup_close", function () {
        $(".awardspopup_info").hide()
    })
    $(".resp-tab-item").on("click", function () {
        var parentid = $(this).parents(".resp-vtabs").attr("id");
        var removelistselector = "#" + parentid + " .resp-tab-item";
        $(removelistselector).removeClass("resp-tab-active");
        $(this).addClass("resp-tab-active");
        var attrselector = $(this).attr("aria-controls");
        var removecontentselector = "#" + parentid + " .resp-tab-content";
        console.log(removecontentselector);
        $(removecontentselector).removeClass("resp-tab-content-active");
        $(removecontentselector).hide();

        var attrcontent = "#" + parentid + " .resp-tab-content[aria-labelledby='" + attrselector + "']";
        console.log(attrcontent);

        $(attrcontent).show();
        $(attrcontent).addClass("resp-tab-content-active");
    });


    $(".resp-accordion").on("click", function () {
        var parentid = $(this).parent().attr("id");
        console.log(parentid);
        var removelistselector = "#" + parentid + " .resp-accordion";
        $(removelistselector).removeClass("resp-tab-active");
        $(this).addClass("resp-tab-active");
        var attrselector = $(this).attr("aria-controls");
        var removecontentselector = "#" + parentid + " .resp-tab-content";
        console.log(removecontentselector);
        $(removecontentselector).removeClass("resp-tab-content-active");
        $(removecontentselector).hide();

        var attrcontent = "#" + parentid + " .resp-tab-content[aria-labelledby='" + attrselector + "']";
        console.log(attrcontent);

        $(attrcontent).show();
        $(attrcontent).addClass("resp-tab-content-active");
    });
    //----------------------------------------//
    //-----Country dropdown in navigation-----//
    //----------------------------------------//

    $('#advanced').flagStrap({
        countries: {
            "VN": "VIET NAM",
            "GB": "UNITED KINGDOM",
            "US": "UNITED STATES",
            "IN": "INDIA",
            "BD": "BANGLADESH",
            "PK": "PAKISTAN"
        },
        buttonType: "btn-flag",
        scrollable: true,
        scrollableHeight: "350px",
        placeholder: {
            text: "VIET NAM"
        }

    });

    //----------------------------------------//
    //--------home-about slider in home page---------//
    //----------------------------------------//
    if ($(".home-page")[0]) {
        if ($(".home_slider")[0]) {
            $('.home_slider').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                arrows: false,
                centerMode: false,
                autoplay: false,
                responsive: [
                    {
                        breakpoint: 1025,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            centerMode: false,
                            dots: false,
                        }
                },

                    {
                        breakpoint: 769,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            centerMode: false,
                            autoplay: false,
                            arrows: true,
                            dots: false
                        }
                },

                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            autoplay: false,
                            arrows: true,
                            dots: false
                        }
                }
            ]
            });
        }

    }
    //----------------------------------------//
    //--------home-about slider in home page---------//
    //----------------------------------------//
    if ($(".water_health-page")[0]) {
        if ($(".health_slider")[0]) {
            $('.health_slider').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                arrows: false,
                centerMode: false,
                autoplay: false,
                responsive: [
                    {
                        breakpoint: 1025,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            centerMode: false,
                            dots: false,
                        }
                },

                    {
                        breakpoint: 769,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            centerMode: false,
                            arrows: true,
                            dots: false,
                        }
                },

                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: true,
                            dots: false,
                        }
                }
            ]
            });
        }
    }
    //----------------------------------------//
    //--------Tab slider in home page---------//
    //----------------------------------------//

    if ($(".tab-slider")[0]) {

        // get the number of .child elements
        var totalitems = $("#accordion .child").length;
        // get the height of .child
        var scrollval = $('.child').height();
        // work out the total height.
        var totalheight = (totalitems * scrollval) - ($("#accordion").height());

        $(document).on("click", "#arrow-down", function () {
            var currentscrollval = $('#accordion').scrollTop();

            $('#accordion').scrollTop(scrollval + currentscrollval);

            // hide/show buttons
            //        if (currentscrollval == totalheight) {
            //            $(this).hide();
            //        } else {
            //            $("#arrow-up").show();
            //        }
        });
        $(document).on("click", "#arrow-up", function () {
            var currentscrollval = parseInt($('#accordion').scrollTop());

            $('#accordion').scrollTop(currentscrollval - scrollval);

            // hide/show buttons
            //        if ((scrollval + currentscrollval) == scrollval) {
            //            $(this).hide();
            //        } else {
            //            $("#arrow-down").show();
            //        }
        });
        $('.collapse').on('shown.bs.collapse', function () {
            $(this).parent().find('a').addClass('active');
        });
        $('.collapse').on('hidden.bs.collapse', function () {
            $(this).parent().find('a').removeClass('active');
        });

    }

    //----------------------------------------//
    //--------Video slider in Clip page---------//
    //----------------------------------------//

    if ($(".video-tab-slider")[0]) {

        $('.slider-youtube').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.video-slider-nav',
            infinite: true,
            autoplay: false,
        });

        $('.video-slider-nav').slick({
            dots: false,
            vertical: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            draggable: true,
            autoPlay: false,
            variableWidth: 200,
            centerMode: false,
            adaptiveHeight: true,
            asNavFor: '.slider-youtube',
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },

                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },

                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        adaptiveHeight: false,
                        autoplay: true,
                        infinite: true,
                        centerMode: false,
                        speed: 900,
                        verticalSwiping: true,
                    }
                }
            ]
        });

        //video slider:Start


        //reset iframe of non current slide
        $('.slider-youtube').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            var current = $(slick.$slides[currentSlide]);
            current.html(current.html());
        });

        //video slider:End

    }


    //----------------------------------------//
    //-- Single page product preview Slider --//
    //----------------------------------------//

    if ($(".slick-for")[0]) {
        $('.slick-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slick-nav',
            adaptiveHeight: 200,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true
                    }
                }]
        });
        $('.slick-nav').slick({
            asNavFor: '.slick-for',
            centerMode: true,
            focusOnSelect: true,
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            arrows: true,
            centerPadding: 10,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },

                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },

                {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        //------------------------------------//
        //------------- Fancy Box ------------//
        //------------------------------------//


        $("[data-fancybox]").each(function () {
            $(this).attr("data-caption", $(this).attr("title"));
        });

        $("[data-fancybox]").fancybox();


    }


    //------------------------------------//
    // Single page product Slider //
    //------------------------------------//

    if ($(".multi_slider")[0]) {
        // Do something if class exists

        $('.multi_slider').slick({

            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
        },

                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
        },

                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
        },

                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
        }

    ]

        });



    }


    //------------------------------------//
    //------------- LightCase ------------//
    //------------------------------------//
    if ($(".lightcase")[0]) {
        jQuery(document).ready(function ($) {
            $('a[data-rel^=lightcase]').lightcase();
        });
    }

    //------------------------------------//
    //--- Single row slider-> Club page --//
    //------------------------------------//
    if ($(".single_row_slider")[0]) {
        $('.single_row_slider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            centerMode: false,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        centerMode: true,
                    }
                },

                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: true,
                    }
                },

                {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    //------------------------------------//
    //--- Single row slider-> Club page --//
    //------------------------------------//
    if ($(".double_row_slider")[0]) {
        $('.double_row_slider').slick({
            slidesPerRow: 1,
            slidesToShow: 2,
            rows: 2,
            arrows: true,
            dots: true,
            customPaging: function (slider, i) {
                i = i + 1;
                return '<a>' + i + '</a>';
            },
            responsive: [

                {
                    breakpoint: 769,
                    settings: {

                        slidesPerRow: 1,
                        slidesToShow: 1,
                        rows: 4,

                    }
            }
        ]
        });

    }


    //------------------------------------//
    //------------- contact form ------------//
    //------------------------------------//
    if ($(".contact-form")[0]) {
        $('.form-input').focus(function () {
            $(this).parent().find(".form-label").addClass('label-active');
        });

        $(".form-input").focusout(function () {
            if ($(this).val() == '') {
                $(this).parent().find(".form-label").removeClass('label-active');
            };
        });
    }

    //----------------------------------------//
    //-- Showroom preview Slider-> location Page --//
    //----------------------------------------//

    if ($(".showroom-for")[0]) {
        $('.showroom-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.showroom-nav',
            adaptiveHeight: 200,
            responsive: [
                {
                    breakpoint: 481,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true,
                    }
                }
            ]
        });
        $('.showroom-nav').slick({
            asNavFor: '.showroom-for',
            centerMode: true,
            focusOnSelect: true,
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            arrows: false,
            centerPadding: 10,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },

                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1.
                    }
                },

                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }
            ]
        });

    }



}); //End of document ready function


//----------------------------------------//
//---------------Lazy Loader--------------//
//----------------------------------------//

document.addEventListener("DOMContentLoaded", function () {
    var lazyloadImages;

    if ("IntersectionObserver" in window) {
        lazyloadImages = document.querySelectorAll(".lazy");
        var imageObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    var image = entry.target;
                    image.src = image.dataset.src;
                    image.classList.remove("lazy");
                    imageObserver.unobserve(image);
                }
            });
        });

        lazyloadImages.forEach(function (image) {
            imageObserver.observe(image);
        });
    } else {
        var lazyloadThrottleTimeout;
        lazyloadImages = document.querySelectorAll(".lazy");

        function lazyload() {
            if (lazyloadThrottleTimeout) {
                clearTimeout(lazyloadThrottleTimeout);
            }

            lazyloadThrottleTimeout = setTimeout(function () {
                var scrollTop = window.pageYOffset;
                lazyloadImages.forEach(function (img) {
                    if (img.offsetTop < (window.innerHeight + scrollTop)) {
                        img.src = img.dataset.src;
                        img.classList.remove('lazy');
                    }
                });
                if (lazyloadImages.length == 0) {
                    document.removeEventListener("scroll", lazyload);
                    window.removeEventListener("resize", lazyload);
                    window.removeEventListener("orientationChange", lazyload);
                }
            }, 20);
        }

        document.addEventListener("scroll", lazyload);
        window.addEventListener("resize", lazyload);
        window.addEventListener("orientationChange", lazyload);
    }
});


//----------------------------------------//
//---------------Contact Form File Upload--------------//
//----------------------------------------//
if ($(".contact-page")[0]) {
    //get upload file name
    function getFileData(myFile) {
        var file = myFile.files[0];
        var filename = file.name;
        document.getElementById("file-name").innerHTML = filename;
    }
}


//----------------------------------------//
//-----Order Page value In/decrementor----//
//----------------------------------------//

function incrementValue() {
    var value = parseInt(document.getElementById('order_number').value, 10);
    value = isNaN(value) ? 0 : value;
    if (value < 10) {
        value++;
        document.getElementById('order_number').value = value;
    }
}

function decrementValue() {
    var value = parseInt(document.getElementById('order_number').value, 10);
    value = isNaN(value) ? 0 : value;
    if (value > 1) {
        value--;
        document.getElementById('order_number').value = value;
    }

}
